[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Ayu-Light
Font=Hack,10,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[Encoding Options]
DefaultEncoding=UTF-8

[General]
Name=Ayu-Light_DEV
Parent=FALLBACK/

[Scrolling]
HistorySize=10000
ScrollBarPosition=1

[Terminal Features]
BlinkingCursorEnabled=true
